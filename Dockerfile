FROM httpd:2.4

#Copia los archivos y carpetas necesarios al directorio de trabajo en el contenedor
COPY css /usr/local/apache2/htdocs/css
COPY html /usr/local/apache2/htdocs/html

#Expone el puerto 81 para que el servidor Apache pueda ser accesible desde el exterior
EXPOSE 81

#Establece el ServerName
RUN echo "ServerName localhost" >> /usr/local/apache2/conf/httpd.conf

#xInicia Apache en el primer plano cuando se ejecute el contenedor
CMD ["httpd", "-D", "FOREGROUND"]
